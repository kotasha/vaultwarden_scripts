#!/bin/bash

set -eo pipefail

VAULTWARDEN_FOLDER=/vw-data
BACKUP_FOLDER=/home/kotasha/vaultwarden_backup
ARCHIVE_PATH=backup.tar.zst
ENC_ARCHIVE_PATH=$ARCHIVE_PATH.enc

cd $BACKUP_FOLDER
# dump, tar, zstd and encrypt
tar cvf - --exclude=$VAULTWARDEN_FOLDER/icon_cache $VAULTWARDEN_FOLDER | zstd -19 -c | gpg --always-trust -r k-rrr@ya.ru -e  > $ENC_ARCHIVE_PATH
# add new backup version
git add $ENC_ARCHIVE_PATH
git commit -m "$(date) vaultwarden backup" --quiet

# strip 40 commits
b="$(git rev-parse --abbrev-ref HEAD)"
h="$(git rev-parse $b)"
echo "Current branch: $b $h"
c="$(git rev-parse $b~39)"
echo "Recreating $b branch with initial commit $c ..."
git checkout --orphan new-start $c --force
echo "commiting"
git commit -C $c
echo "rebasing"
git rebase --onto new-start $c $b
echo "making new branch"
git branch -d new-start
echo "garbage collection"
git reflog expire --expire=now --all
git gc --prune=now
# push
echo "pushing"
git push -u origin main --force
