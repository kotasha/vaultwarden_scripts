# nginx config
map $http_upgrade $connection_upgrade {
    default upgrade;
    ''      close;
}

server {
        listen       443 ssl http2;
        listen       [::]:443 ssl http2;        
	server_name  vault.thereisno.ml;
	add_header Allow "GET" always; 
	ssl_certificate /etc/ssl/thereisno.ml/thereisno.ml.cert;
	ssl_certificate_key /etc/ssl/thereisno.ml/thereisno.ml.key;
	ssl_dhparam /etc/ssl/dhparams.pem;
	ssl_session_cache shared:le_nginx_SSL:1m;
	ssl_session_cache shared:SSL:50m;
	ssl_session_timeout 1d;
	ssl_session_tickets off;
	ssl_protocols TLSv1.3;	
	ssl_ecdh_curve secp384r1;
	ssl_early_data on;		
	add_header Content-Security-Policy "default-src https: data: 'unsafe-inline' 'unsafe-eval'" always;
	add_header Strict-Transport-Security 'max-age=63072000; includeSubdomains; preload' always;
	add_header X-Robots-Tag "noindex, nofollow" always;	
	add_header X-Content-Type-Options "nosniff" always;
	add_header X-Frame-Options "SAMEORIGIN" always;
	add_header X-Xss-Protection "1; mode=block" always;
	resolver localhost valid=300s;
	ssl_buffer_size 8k;
	ssl_stapling on;
	ssl_stapling_verify on;
	ssl_ciphers TLS13-CHACHA20-POLY1305-SHA256:TLS13-AES-256-GCM-SHA384:TLS13-AES-128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384;
	ssl_prefer_server_ciphers on;
        ssl_trusted_certificate /etc/ssl/certs/cloudflare.pem;
	# vaultwarden
	location / {
		proxy_set_header Host $host;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header X-Forwarded-Proto $scheme;
		proxy_pass http://127.0.0.1:60080;
	}
	location /notifications/hub {
   		proxy_pass http://127.0.0.1:63012;
    		proxy_set_header Upgrade $http_upgrade;
    		proxy_set_header Connection "upgrade";
  	}
  	location /notifications/hub/negotiate {
    		proxy_pass http://127.0.0.1:60080;
  	}
}
