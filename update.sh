#!/bin/bash

sudo docker kill vaultwarden
sudo docker rm vaultwarden
sudo docker pull vaultwarden/server:latest
sudo docker run -d --name vaultwarden \
                --restart unless-stopped \
                -v /vw-data/:/data/ \
                -p 60080:80 \
                -p 63012:3012 \
                vaultwarden/server:latest
